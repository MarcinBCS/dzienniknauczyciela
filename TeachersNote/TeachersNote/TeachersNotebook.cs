﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TeachersNote
{
    internal class TeachersNotebook
    {
        private List<Student> StudentList;
        private int Id;
        private string Letter;

        public TeachersNotebook(int id, string letter)
        {
            this.Id = id;
            this.Letter = letter;

            StudentList = new List<Student>();
        }

        public void AddStudent(string name, string lastName, string pesel, string street, string town, string code, string phone)
        {
            StudentList.Add(new Student(Guid.NewGuid(), name, lastName, pesel, street, town, code, phone));
            Console.WriteLine("#Dodano ucznia:" + name);
        }

        public void SortByLastName()
        {
            StudentList = StudentList.OrderBy(x => x.LastName).ToList();
        }


        public void AddDegree(int id, int subject, int degree)
        {
            StudentList[id].AddDegree(subject, degree);
        }

        public void ShowStudents()
        {
            for (int i = 0; i < StudentList.Count; i++)
            {
                StudentList[i].ShowData(i);
            }

        }

        public void ShowStudentsSubjectAverage(int subject)
        {
            for (int i = 0; i < StudentList.Count; i++)
            {
                StudentList[i].ShowSubjectAverage(subject, i);
            }
        }

        public void ShowStudentsTotalAverage()
        {
            for (int i = 0; i < StudentList.Count; i++)
            {
                StudentList[i].ShowTotalAverage(i);
            }
        }

        public void EditStudent(int id, string name, string lastName, string pesel, string street, string town, string code, string phone)
        {
            
            Student student = StudentList[id];
            student.Name = name;
            student.LastName = lastName;
            student.Pesel = pesel;
            student.Street = street;
            student.Town = town;
            student.Code = code;
            student.Phone = phone;

            StudentList[id] = student;
        }

        public void DeleteStudent(int id)
        {
            StudentList.Remove(StudentList[id]);
        }

        public double GetTotalAverage()
        {
            return StudentList.Average(x => x.GetTotalAverage());
        }

        public double GetSubjectAverage(int id)
        {
            return StudentList.Average(x => x.GetSubjectAverage(id));
        }
    }
}