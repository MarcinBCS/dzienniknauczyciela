﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachersNote
{
    class Program
    {
        static void Main(string[] args)
        {
            TeachersNotebook tc = new TeachersNotebook(1,"c");

            tc.AddStudent("Marcin", "Włoch", "78010129187", "ul. 3 Maja 7", "Lublin", "20-834", "745745898");
            tc.AddStudent("Adam", "Małysz", "61012664615", "ul. Jana Pawła 2 45/99", "Lublin", "20-843", "789456123");
            tc.AddStudent("Zenon", "Laskowik", "44020211615", "ul. Piłsudskiego 3/3", "Lublin", "20-825", "566478987");
            tc.AddStudent("Krzysztof", "Krawczyk", "51041942989", "ul. Piwna 45/5", "Lublin", "20-801", "54778789");
            tc.AddStudent("Anna", "Dymna", "63082181911", "ul. Złota 4", "Lublin", "20-894", "798754456");

            Console.WriteLine("POKAZUJE STUDENTOW\n");
            tc.ShowStudents();

            Console.WriteLine("\nSORTUJE STUDENTOW\n");
            tc.SortByLastName();

            Console.WriteLine("\nWYSWIETLAM STUDENTOW\n");
            tc.ShowStudents();

            //polski id 1
            //matma id 2

            //id, przedmiot, ocena
            tc.AddDegree(1, 1, 2);
            tc.AddDegree(1, 1, 3);
            tc.AddDegree(1, 1, 2);
            tc.AddDegree(1, 1, 3);
            tc.AddDegree(1, 1, 2);
            tc.AddDegree(1, 1, 3);
            tc.AddDegree(1, 1, 2);

            tc.AddDegree(1, 2, 2);
            tc.AddDegree(1, 2, 3);
            tc.AddDegree(1, 2, 4);
            tc.AddDegree(1, 2, 5);
            tc.AddDegree(1, 2, 3);
            tc.AddDegree(1, 2, 5);
            tc.AddDegree(1, 2, 6);

            tc.AddDegree(2, 1, 3);
            tc.AddDegree(2, 1, 4);
            tc.AddDegree(2, 1, 2);
            tc.AddDegree(2, 1, 4);
            tc.AddDegree(2, 1, 3);
            tc.AddDegree(2, 1, 6);
            tc.AddDegree(2, 1, 4);

            tc.AddDegree(2, 2, 2);
            tc.AddDegree(2, 2, 3);
            tc.AddDegree(2, 2, 4);
            tc.AddDegree(2, 2, 6);
            tc.AddDegree(2, 2, 5);
            tc.AddDegree(2, 2, 2);
            tc.AddDegree(2, 2, 3);

            Console.WriteLine("\nSREDNIA STUDENTOW\n");
            tc.ShowStudentsTotalAverage();

            Console.WriteLine("\nSREDNIA STUDENTOW Z POLSKIEGO\n");
            tc.ShowStudentsSubjectAverage(1);

            Console.WriteLine("\nSREDNIA STUDENTOW Z MATEMATYKI\n");
            tc.ShowStudentsSubjectAverage(2);


            Console.WriteLine("\nZMIANA DANYCH\n");
            tc.EditStudent(2, "Krzystof", "Gmoch", "8888888888", "ul. Jana Pawła 2", "Lublin", "20-814", "777777777");
            tc.ShowStudentsSubjectAverage(2);

            Console.Read();
        }
    }
}
