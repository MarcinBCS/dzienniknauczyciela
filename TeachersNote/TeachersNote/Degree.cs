﻿namespace TeachersNote
{
    public class Degree
    {
        public int Subject;
        public int Number;

        public Degree(int subject, int number)
        {
            this.Subject = subject;
            this.Number = number;
        }
    }
}