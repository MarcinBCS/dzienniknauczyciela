﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TeachersNote
{
    public class Student
    {
        public Guid UniqueId;
        public string Name;
        public string LastName;
        public string Pesel;
        public string Street;
        public string Town;
        public string Code;
        public string Phone;

        List<Degree> ListDegree;

        public Student(Guid uniqueId, string name, string lastName, string pesel, string street, string town, string code, string phone)
        {
            this.UniqueId = uniqueId;
            this.Name = name;
            this.LastName = lastName;
            this.Pesel = pesel;
            this.Street = street;
            this.Town = town;
            this.Code = code;
            this.Phone = phone;

            ListDegree = new List<Degree>();
        }

        public void AddDegree(int subject, int degree)
        {
            ListDegree.Add(new Degree(subject, degree));
        }

        public void ShowData(int i)
        {
            Console.WriteLine("{0}. {1} {2} {3} {4} {5} {6} {7} \n UID: {8}", i, Name, LastName, Pesel, Street, Town, Code, Phone, UniqueId);
        }

        public void ShowSubjectAverage(int subject, int i)
        {
            if (ListDegree.Count > 0)
                Console.WriteLine("{0}. {1} {2} : {3}", i, Name, LastName, ListDegree.Where(x => x.Subject == subject).ToList().Average(x => x.Number));
        }

        public void ShowTotalAverage(int i)
        {
            if(ListDegree.Count > 0)
             Console.WriteLine("{0}. {1} {2} : {3}", i, Name, LastName, ListDegree.Average(x => x.Number));
        }

        public double GetTotalAverage()
        {
            return ListDegree.Average(x => x.Number);
        }

        public double GetSubjectAverage(int subject)
        {
            return ListDegree.Where(x => x.Subject == subject).ToList().Average(x => x.Number);
        }
    }
}